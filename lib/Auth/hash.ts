import * as argon2 from 'argon2';

export async function HashPassword(password: string): Promise<string> {
  return await argon2.hash(password);
}

export async function VerifyPassword(
  password: string,
  hash: string,
): Promise<boolean> {
  return await argon2.verify(hash, password);
}
