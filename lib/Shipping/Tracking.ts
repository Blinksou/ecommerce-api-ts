export default function GetOrderTracking(orderId: string) {
  const expectedDeliveryDate = getDatesBetweenDates(
    new Date(),
    new Date(new Date().setDate(new Date().getDate() + 7)),
  );

  return {
    expectedDeliveryDate:
      expectedDeliveryDate[
        Math.floor(Math.random() * expectedDeliveryDate.length)
      ].toLocaleDateString('fr-FR'),
    deliverer: 'DHL',
    trackingNumber: `CU${Math.floor(10000000 + Math.random() * 90000000)}DE`,
  };
}

const getDatesBetweenDates = (startDate: Date, endDate: Date) => {
  let dates = [];
  //to avoid modifying the original date
  const theDate = new Date(startDate);
  while (theDate < endDate) {
    dates = [...dates, new Date(theDate)];
    theDate.setDate(theDate.getDate() + 1);
  }
  dates = [...dates, endDate];
  return dates;
};
