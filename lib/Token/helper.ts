import { v4 } from 'uuid';
let cuid = require('cuid');

export default function GenerateToken(): string {
  return v4();
}

export function generateId(): string {
  return cuid();
}
