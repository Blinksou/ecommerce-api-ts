import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import ProductModule from '../../src/product/product.module';
import ProductService from '../../src/product/product.service';

describe('Products', () => {
  let app: INestApplication;
  const productService = { findAll: () => `This action returns all product` };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ProductModule],
    })
      .overrideProvider(ProductService)
      .useValue(ProductService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  /* it(`/GET products`, () => {
    return request(app.getHttpServer()).get('/product').expect(200);
  }); */

  afterAll(async () => {
    await app.close();
  });
});
