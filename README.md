# Ecommerce-api-ts

SaaS E commerce API

## Installation

```bash
$ npm install
```

### Prisma

```bash
$ npx prisma generate
$ npx prisma migrate dev

# pour créer une migration
$ npx prisma migrate dev --name NOM_DE_LA_MIGRATION

# pour synchro simplement la DB & generer les schema prisma
$ npx prisma db push && npx prisma generate

# pour seed la DB
$ npx prisma db seed
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
