import { HttpException, HttpStatus } from '@nestjs/common';

export class OrderNotFoundException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Order not Found', code ?? HttpStatus.NOT_FOUND);
  }
}
