import { Module } from '@nestjs/common';
import { CartModule } from 'src/cart/cart.module';
import { StoreModule } from 'src/store/store.module';

import OrderController from './order.controller';
import OrderService from './order.service';

@Module({
  controllers: [OrderController],
  providers: [OrderService],
  imports: [StoreModule, CartModule],
  exports: [OrderService],
})
export default class OrderModule {}
