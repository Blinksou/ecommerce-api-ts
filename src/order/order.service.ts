import { Injectable } from '@nestjs/common';
import { OrderStatus, Prisma, User } from '@prisma/client';
import GetOrderTracking from 'lib/Shipping/Tracking';
import { CartService } from 'src/cart/cart.service';
import { MailService } from 'src/mail/mail.service';
import PrismaService from 'src/prisma/prisma.service';
import { Pagination } from 'src/types/pagination';

import CreateOrderDto from './dto/create-order.dto';
import UpdateOrderDto from './dto/update-order.dto';
import { OrderNotFoundException } from './exception/order-not-found.exception';

@Injectable()
export default class OrderService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly cartService: CartService,
    private readonly mailService: MailService,
  ) {}

  public async create(createOrderDto: CreateOrderDto, owner: User) {
    const cart = await this.cartService.findCartOfUserByStore(
      createOrderDto.storeId,
      owner,
    );
    if (cart != null) {
      return await this.prisma.order.create({
        data: {
          ...createOrderDto,
          createdAt: new Date(),
          status: OrderStatus.OPEN,
          cartId: cart.id,
          userId: owner.id,
          sessionId: null,
        },
      });
    }
    return null;
  }

  public async findAllByUser(
    owner: User,
    storeId: string,
    pagination: Pagination,
  ) {
    return await this.prisma.order.findMany({
      where: { userId: owner.id, storeId },
      skip: pagination.skip,
      take: pagination.take,
    });
  }

  public async findAllByStore(storeId: string, pagination?: Pagination) {
    return await this.prisma.order.findMany({
      where: { storeId },
      skip: pagination?.skip,
      take: pagination?.take,
    });
  }

  public async findOne(id: string) {
    return await this.prisma.order.findUnique({
      where: { id },
    });
  }

  public async update(id: string, orderInput: Prisma.OrderUpdateInput) {
    if (orderInput.status == OrderStatus.SHIPPED) {
      let order = await this.prisma.order.findUnique({
        where: {
          id,
        },
        include: { user: true },
      });

      let user = await this.prisma.user.findFirst({
        where: { id: order.userId },
      });

      await this.mailService.sendOrderExpeditionConfirmation(order.user, order);
    }
    return await this.prisma.order.update({
      where: {
        id,
      },
      data: {
        ...orderInput,
      },
    });
  }

  public async updateStatus(id: string, updateOrderDto: UpdateOrderDto) {
    return await this.prisma.order.update({
      where: {
        id,
      },
      data: {
        status: updateOrderDto.status,
      },
    });
  }

  public async remove(id: string) {
    return await this.prisma.order.delete({
      where: { id },
    });
  }

  public async validateOrder(id: string) {
    const order = await this.prisma.order.findUnique({
      where: {
        id,
      },
      include: {
        cart: {
          include: {
            items: {
              include: {
                product: true,
              },
            },
          },
        },
        user: true,
        store: true,
      },
    });

    if (!order) {
      throw new OrderNotFoundException();
    }

    return order;
  }

  public async findLastByUserAndStore(id: string, storeId: string) {
    const order = await this.prisma.order.findFirst({
      where: {
        userId: id,
        cart: {
          storeId,
        },
      },
      include: {
        cart: {
          include: {
            items: {
              include: {
                product: true,
              },
            },
          },
        },
        user: true,
        store: true,
      },
    });

    return order ?? null;
  }

  public async handleUpdateOrderByOwnerRequest(
    id: string,
    updateOrderDto: UpdateOrderDto,
    user: User,
  ): Promise<object> {
    const order = await this.validateOrder(id);

    if (user.id !== order.store.ownerId) {
      throw new Error('Only store owners can perform this action');
    }

    await this.updateStatus(order.id, updateOrderDto);

    if (updateOrderDto.status == OrderStatus.SHIPPED) {
      await this.mailService.sendOrderExpeditionConfirmation(order.user, order);
    }

    return GetOrderTracking(order.id);
  }
}
