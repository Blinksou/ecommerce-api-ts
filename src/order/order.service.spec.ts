import { Test, TestingModule } from '@nestjs/testing';
import { Order, User } from '@prisma/client';
import GenerateToken, { generateId } from '../../lib/Token/helper';
import { prismaMock } from '../../prisma/singleton';
import prisma from '../../prisma/client';
import OrderService from './order.service';
import PrismaModule from 'src/prisma/prisma.module';
import CreateOrderDto from './dto/create-order.dto';
import UpdateOrderDto from './dto/update-order.dto';

describe('OrderService', () => {
  let service: OrderService;
  let defaultOrder: Order;
  defaultOrder = {
    id: generateId(),
    storeId: generateId(),
    cartId: generateId(),
    userId: generateId(),
    createdAt: new Date(),
    updatedAt: new Date(),
    status: 'OPEN',
    sessionId: null,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [OrderService],
    }).compile();

    service = module.get<OrderService>(OrderService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of orders', async () => {
      expect(Array.isArray(await service.findAllByorder(generateId()))).toBe(
        true,
      );
    });
  });

  describe('Create', () => {
    it('should create a new order', async () => {
      const orderDto = new CreateOrderDto();
      orderDto.storeId = generateId();

      let user: User;
      user = {
        id: generateId(),
        email: 'hello@test.io',
        password: 'password',
        role: 'USER',
      };

      let expectedOrder: Partial<Order>;
      expectedOrder = {
        storeId: orderDto.storeId,
        userId: user.id,
      };

      //@ts-ignore
      const cart = await prismaMock.cart.findFirst();

      //@ts-ignore
      prismaMock.order.create.mockResolvedValue(expectedOrder);

      await expect(
        prisma.order.create({
          data: {
            ...orderDto,
            createdAt: new Date(),
            status: 'OPEN',
            cartId: cart.id,
            userId: user.id,
            sessionId: null,
          },
        }),
      ).resolves.toEqual(expectedOrder);
    });
  });

  describe('Find One', () => {
    it('should find one order', async () => {
      let expectedOrder = defaultOrder;

      //@ts-ignore
      prismaMock.order.findUnique.mockResolvedValue(expectedOrder);

      await expect(
        prisma.order.findUnique({
          where: {
            id: expectedOrder.id,
          },
        }),
      ).resolves.toEqual(expectedOrder);
    });
  });

  describe('Update', () => {
    it('should update one store', async () => {
      const orderDto = new UpdateOrderDto();
      orderDto.status = 'PAID';
      orderDto.storeId = generateId();

      let expectedOrder = defaultOrder;
      expectedOrder.status = orderDto.status;
      expectedOrder.storeId = orderDto.storeId;

      //@ts-ignore
      prismaMock.order.update.mockResolvedValue(expectedOrder);

      await expect(
        prisma.order.update({
          where: { id: expectedOrder.id },
          data: orderDto,
        }),
      ).resolves.toEqual(expectedOrder);
    });
  });
});
