import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Inject,
  Param,
  Patch,
  Post,
  Request,
  Res,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserRole } from '@prisma/client';
import { Response } from 'express';
import RequestWithUser from 'src/auth/request-with-user.interface';
import { GetPagination } from 'src/decorators/get-pagination';
import { StoreService } from 'src/store/store.service';
import { STRIPE_CLIENT } from 'src/stripe/constants';
import { Pagination } from 'src/types/pagination';
import Stripe from 'stripe';

import CreateOrderDto from './dto/create-order.dto';
import UpdateOrderDto from './dto/update-order.dto';
import { OrderNotFoundException } from './exception/order-not-found.exception';
import OrderService from './order.service';

@Controller('order')
@ApiTags('Order')
export default class OrderController {
  constructor(
    private readonly orderService: OrderService,
    private readonly storeService: StoreService,
    @Inject(STRIPE_CLIENT) private readonly stripe: Stripe,
  ) {}

  @Post()
  @ApiBearerAuth()
  public async create(
    @Body() createOrderDto: CreateOrderDto,
    @Request() req: RequestWithUser,
  ) {
    return await this.orderService.create(createOrderDto, req.user);
  }

  @Get('store/:storeId')
  @ApiBearerAuth()
  public async findAllByStore(
    @Param('storeId') storeId: string,
    @GetPagination() pagination: Pagination,
    @Request() req: RequestWithUser,
  ) {
    // Get store
    const store = await this.storeService.validateStore(storeId);

    // if user is NOT admin and is NOT the store owner, then find only orders of the user in this store
    if (req.user.role !== UserRole.ADMIN && req.user.id !== store.ownerId) {
      return await this.orderService.findAllByUser(
        req.user,
        storeId,
        pagination,
      );
    }

    return await this.orderService.findAllByStore(storeId, pagination);
  }

  @Get(':id')
  @ApiBearerAuth()
  public async findOne(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    const order = await this.orderService.validateOrder(id);

    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== order.userId &&
      req.user.id !== order.store.ownerId
    ) {
      throw new OrderNotFoundException();
    }

    return order;
  }

  @Patch(':id')
  @ApiBearerAuth()
  public async update(
    @Param('id') id: string,
    @Body() updateOrderDto: UpdateOrderDto,
    @Request() req: RequestWithUser,
  ) {
    const order = await this.orderService.validateOrder(id);

    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== order.userId &&
      req.user.id !== order.store.ownerId
    ) {
      throw new OrderNotFoundException();
    }

    return await this.orderService.updateStatus(id, updateOrderDto);
  }

  @Patch('/owner/:id')
  @ApiBearerAuth()
  public async updateOrderByOwner(
    @Param('id') id: string,
    @Body() updateOrderDto: UpdateOrderDto,
    @Request() req: RequestWithUser,
    @Res() res: Response,
  ) {
    try {
      const orderInfos =
        await this.orderService.handleUpdateOrderByOwnerRequest(
          id,
          updateOrderDto,
          req.user,
        );

      return res.status(HttpStatus.OK).json({
        data: orderInfos,
      });
    } catch (error: any) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: error,
      });
    }
  }
}
