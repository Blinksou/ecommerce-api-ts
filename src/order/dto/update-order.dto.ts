import { ApiProperty, PartialType } from '@nestjs/swagger';
import { OrderStatus } from '@prisma/client';
import { IsNotEmpty } from 'class-validator';

import CreateOrderDto from './create-order.dto';

export default class UpdateOrderDto extends PartialType(CreateOrderDto) {
  @IsNotEmpty()
  @ApiProperty()
  status?: OrderStatus;
}
