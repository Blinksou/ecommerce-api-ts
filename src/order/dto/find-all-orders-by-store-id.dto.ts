import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class FindAllOrdersByStoreId {
  @IsNotEmpty()
  @ApiProperty()
  storeId!: string;
}
