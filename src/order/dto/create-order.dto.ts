import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class CreateOrderDto {
  @IsNotEmpty()
  @ApiProperty()
  storeId!: string;
}
