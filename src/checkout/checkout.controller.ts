import { BadRequestException, Controller, Get, Inject, Param, Render, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiExcludeEndpoint, ApiTags } from '@nestjs/swagger';
import { OrderStatus } from '@prisma/client';
import { Public } from 'src/auth/decorator/public.decorator';
import { PermissionRequiredException } from 'src/auth/exception/permission-required.exception';
import RequestWithUser from 'src/auth/request-with-user.interface';
import OrderService from 'src/order/order.service';
import ProductService from 'src/product/product.service';
import { STRIPE_CLIENT } from 'src/stripe/constants';
import Stripe from 'stripe';

@Controller('checkout')
@ApiTags('Checkout')
export default class CheckoutController {
  constructor(
    private readonly orderService: OrderService,
    private readonly productService: ProductService,
    @Inject(STRIPE_CLIENT) private readonly stripe: Stripe,
  ) {}

  @Get('stripe/:orderId')
  @ApiBearerAuth()
  public async createStripeCheckoutSessionForOrder(
    @Param('orderId') orderId: string,
    @Request() req: RequestWithUser,
  ) {
    const order = await this.orderService.validateOrder(orderId);

    if (order.userId !== req.user.id) {
      throw new PermissionRequiredException();
    }

    try {
      const session = await this.stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        mode: 'payment',
        line_items: order.cart.items.map((orderItem) => {
          return {
            quantity: orderItem.quantity,
            price_data: {
              currency: 'eur',
              product_data: {
                name: orderItem.product.name,
              },
              unit_amount: orderItem.product.price * 100,
            },
          };
        }),
        success_url: `${process.env.APP_URL}/checkout/stripe/success/${orderId}`,
        cancel_url: `${process.env.APP_URL}/checkout/stripe/cancel/${orderId}`,
      });

      await this.orderService.update(orderId, {
        status: OrderStatus.PROCESSING,
        sessionId: session.id,
      });

      return {
        url: session.url,
      };
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  @Get('stripe/success/:orderId')
  @Render('checkout/index')
  @Public()
  @ApiExcludeEndpoint()
  public async successView(@Param('orderId') orderId: string) {
    const order = await this.orderService.validateOrder(orderId);

    const sessionInfos = await this.stripe.checkout.sessions.retrieve(
      order.sessionId,
    );

    if (
      sessionInfos.payment_status === 'paid' ||
      sessionInfos.payment_status === 'no_payment_required'
    ) {
      await this.orderService.update(orderId, {
        status: OrderStatus.PAID,
      });

      await this.productService.updateQuantityAfterOrder(order.cart.items);
    }

    return {
      message:
        'Thank you for ordering with us ! (This page should redirect to the store page in the future, not implemented yet)',
      orderId: orderId,
      orderStatus: order.status,
    };
  }

  @Get('stripe/cancel/:orderId')
  @Render('checkout/index')
  @Public()
  @ApiExcludeEndpoint()
  public async cancelView(@Param('orderId') orderId: string) {
    const order = await this.orderService.validateOrder(orderId);

    await this.orderService.update(orderId, {
      status: OrderStatus.OPEN,
      sessionId: null,
    });

    return {
      message:
        'Order successfully canceled ! (This page should redirect to the store page in the future, not implemented yet)',
      orderId: orderId,
      orderStatus: order.status,
    };
  }
}
