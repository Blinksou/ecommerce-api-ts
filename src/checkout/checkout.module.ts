import { Module } from '@nestjs/common';
import OrderModule from 'src/order/order.module';
import ProductModule from 'src/product/product.module';

import CheckoutController from './checkout.controller';

@Module({
  controllers: [CheckoutController],
  imports: [OrderModule, ProductModule],
})
export default class CheckoutModule {}
