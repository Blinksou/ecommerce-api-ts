import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

import { Pagination } from '../types/pagination';

export const GetPagination = createParamDecorator(
  (data, ctx: ExecutionContext): Pagination => {
    const req: Request = ctx.switchToHttp().getRequest();

    const paginationParams: Pagination = {
      skip: 0,
      take: 30,
    };

    paginationParams.skip = req.query.skip
      ? parseInt(req.query.skip.toString())
      : 0;
    paginationParams.take = req.query.take
      ? parseInt(req.query.take.toString())
      : 30;

    return paginationParams;
  },
);
