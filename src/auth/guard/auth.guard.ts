import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

// Usable with @Auth(ROLE) in controller
// If you dont put a role, it will return true
@Injectable()
export default class AuthGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const role = this.reflector.get<string | null>(
      'role',
      context.getHandler(),
    );

    if (!role) {
      return true;
    }

    const request = context.switchToHttp().getRequest();

    const user = request.user;

    if (!user.role) {
      return false;
    }

    return this.matchRole(role, user.role);
  }

  private matchRole(role: string, userRole: string): boolean {
    if (role.toUpperCase() === userRole.toUpperCase()) {
      return true;
    }

    return false;
  }
}
