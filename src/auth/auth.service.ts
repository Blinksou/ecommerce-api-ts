import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';

import UserService from '../user/user.service';
import { LoginUserDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register.dto';
import { HashPassword, VerifyPassword } from '../../lib/Auth/hash';
import { InvalidCredentialsException } from './exception/invalid-credentials.exception';

@Injectable()
export default class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  public async login(user: LoginUserDto): Promise<object> {
    const userInDb = await this.userService.findOne({ email: user.email });

    if (!userInDb || !VerifyPassword(user.password, userInDb.password)) {
      throw new InvalidCredentialsException();
    }

    const payload = {
      id: userInDb.id,
      email: user.email,
      role: userInDb.role,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  public async register(user: RegisterUserDto): Promise<User> {
    user.password = await HashPassword(user.password);

    return await this.userService.create(user);
  }

  public async findByEmail(email: string): Promise<User | null> {
    return await this.userService.findOne({ email });
  }
}
