import { HttpException, HttpStatus } from '@nestjs/common';

export class InvalidCredentialsException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(
      msg ?? 'Username or password did not match',
      code ?? HttpStatus.BAD_REQUEST,
    );
  }
}
