import { HttpException, HttpStatus } from '@nestjs/common';

export class PermissionRequiredException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(
      msg ?? 'You are not allowed to do this action',
      code ?? HttpStatus.BAD_REQUEST,
    );
  }
}
