import { HttpException, HttpStatus } from '@nestjs/common';

export class EmailAlreadyInUseException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(
      msg ?? 'Email is already in use',
      code ?? HttpStatus.UNPROCESSABLE_ENTITY,
    );
  }
}
