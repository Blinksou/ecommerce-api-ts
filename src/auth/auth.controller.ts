import { Body, Controller, Get, Post, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import AuthService from './auth.service';
import { Public } from './decorator/public.decorator';
import { LoginUserDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register.dto';
import { EmailAlreadyInUseException } from './exception/email-already-in-use.exception';
import RequestWithUser from './request-with-user.interface';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @Public()
  public async registerUser(@Body() user: RegisterUserDto) {
    const emailExists = await this.authService.findByEmail(user.email);

    if (emailExists) {
      throw new EmailAlreadyInUseException();
    }

    return await this.authService.register(user);
  }

  @Post('login')
  @Public()
  public async loginUser(@Body() user: LoginUserDto) {
    return await this.authService.login(user);
  }

  @Get('me')
  @ApiBearerAuth()
  public async getLoggedInUser(@Request() req: RequestWithUser) {
    return req.user;
  }
}
