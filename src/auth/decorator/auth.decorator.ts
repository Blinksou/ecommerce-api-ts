import { applyDecorators, SetMetadata } from '@nestjs/common';

export function Auth(role: string | null) {
  return applyDecorators(SetMetadata('role', role));
}
