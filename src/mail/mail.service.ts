import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Order, User } from '@prisma/client';
import GetOrderTracking from 'lib/Shipping/Tracking';

type ShippingDetails = {
  expectedDeliveryDate: string;
  deliverer: string;
  trackingNumber: string;
};

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  public async sendOrderExpeditionConfirmation(
    user: User,
    order: Order,
    details?: ShippingDetails,
  ) {
    try {
      await this.mailerService.sendMail({
        to: user.email,
        subject: 'Votre commande à bien été envoyée !',
        template: 'confirmationExpedition',
        context: {
          name: user.email,
          orderId: order.id,
          details: details ?? GetOrderTracking(order.id),
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
