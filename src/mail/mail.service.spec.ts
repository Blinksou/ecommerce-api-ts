import { Test, TestingModule } from '@nestjs/testing';
import { Order, User } from '@prisma/client';
import { generateId } from '../../lib/Token/helper';
import { MailModule } from './mail.module';
import { MailService } from './mail.service';

describe('MailService', () => {
  let service: MailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [MailModule],
      providers: [MailService],
    }).compile();

    service = module.get<MailService>(MailService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('sould send an email', () => {
    let user: User;
    user = {
      id: generateId(),
      email: 'blabla@io.com',
      password: 'password',
      role: 'USER',
    };

    let order: Order;
    order = {
      id: generateId(),
      storeId: generateId(),
      cartId: generateId(),
      userId: user.id,
      createdAt: new Date(),
      updatedAt: new Date(),
      status: 'PAID',
      sessionId: null,
    };
    expect(service.sendOrderExpeditionConfirmation(user, order)).resolves;
  });
});
