import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Request,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserRole } from '@prisma/client';
import { Auth } from 'src/auth/decorator/auth.decorator';
import { Public } from 'src/auth/decorator/public.decorator';
import { PermissionRequiredException } from 'src/auth/exception/permission-required.exception';
import RequestWithUser from 'src/auth/request-with-user.interface';
import { CartService } from 'src/cart/cart.service';
import { GetPagination } from 'src/decorators/get-pagination';
import { Pagination } from 'src/types/pagination';

import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';
import { StoreService } from './store.service';

@Controller('store')
@ApiTags('Store')
export class StoreController {
  constructor(
    private readonly cartService: CartService,
    private readonly storeService: StoreService,
  ) {}

  @Post()
  @ApiBearerAuth()
  @ApiResponse({
    status: 201,
    description: 'The store has been successfully created',
  })
  @ApiResponse({ status: 204, description: 'No content' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  @ApiResponse({ status: 403, description: 'Forbidden' })
  @ApiParam({
    name: 'Name',
    required: true,
    description: 'Name of the store',
    type: 'string',
  })
  @ApiParam({
    name: 'Key',
    required: true,
    description: 'Store key',
    type: 'string',
  })
  @ApiParam({
    name: 'StoreAddress',
    required: true,
    description: 'Store address',
    type: 'string',
  })
  public async create(
    @Body() createStoreDto: CreateStoreDto,
    @Request() req: RequestWithUser,
  ) {
    return await this.storeService.create(createStoreDto, req.user);
  }

  @Get()
  @ApiBearerAuth()
  @Auth('admin')
  @ApiResponse({ status: 400, description: 'Bad request' })
  @ApiResponse({ status: 403, description: 'Forbidden' })
  public async findAll(@GetPagination() pagination: Pagination) {
    return await this.storeService.findAll(pagination);
  }

  @Get(':id')
  @Public()
  @ApiOkResponse()
  @ApiResponse({ status: 400, description: 'Bad request' })
  @ApiResponse({ status: 403, description: 'Forbidden' })
  public async findOne(@Param('id') id: string) {
    return await this.storeService.findOne(id);
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiResponse({ status: 201, description: 'The store has been updated' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  @ApiResponse({ status: 403, description: 'Forbidden' })
  public async update(
    @Param('id') id: string,
    @Body() updateStoreDto: UpdateStoreDto,
    @Request() req: RequestWithUser,
  ) {
    // get store
    const store = await this.storeService.validateStore(id);

    // check if store is to current logged user
    if (req.user.role !== UserRole.ADMIN && req.user.id !== store.ownerId) {
      throw new PermissionRequiredException();
    }

    return await this.storeService.update(id, updateStoreDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiResponse({ status: 201, description: 'The store has been deleted' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  @ApiResponse({ status: 403, description: 'Forbidden' })
  public async remove(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    // get store
    const store = await this.storeService.validateStore(id);

    // check if store is to current logged user
    if (req.user.role !== UserRole.ADMIN && req.user.id !== store.ownerId) {
      throw new PermissionRequiredException();
    }

    return await this.storeService.remove(id);
  }

  @Get(':id/cart')
  @ApiBearerAuth()
  public async findCartOfUserByStore(
    @Param('id') storeId: string,
    @Request() req: RequestWithUser,
  ) {
    await this.storeService.validateStore(storeId);

    return await this.cartService.findCartOfUserByStore(storeId, req.user);
  }

  @Delete(':id/cart')
  @ApiBearerAuth()
  public async cancelCartOfUserByStore(
    @Param('id') storeId: string,
    @Request() req: RequestWithUser,
  ) {
    await this.storeService.validateStore(storeId);

    const cart = await this.cartService.findCartOfUserByStore(
      storeId,
      req.user,
    );

    return await this.cartService.remove(cart.id);
  }
}
