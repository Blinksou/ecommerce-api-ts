import { Test, TestingModule } from '@nestjs/testing';
import UserService from '../user/user.service';
import PrismaModule from '../prisma/prisma.module';
import { CreateStoreDto } from './dto/create-store.dto';
import { StoreService } from './store.service';
import { User, Store } from '@prisma/client';
import GenerateToken, { generateId } from '../../lib/Token/helper';
import { prismaMock } from '../../prisma/singleton';
import prisma from '../../prisma/client';
import { UpdateStoreDto } from './dto/update-store.dto';

describe('StoreService', () => {
  let service: StoreService;
  let userService: UserService;
  let defaultStore: Store;
  defaultStore = {
    id: generateId(),
    name: 'Store test',
    key: GenerateToken(),
    storeAdress: '/store-addr',
    ownerId: generateId(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [StoreService, UserService],
    }).compile();

    service = module.get<StoreService>(StoreService);
    userService = module.get<UserService>(UserService);

    prisma.store.create({
      data: defaultStore,
    });
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of stores', async () => {
      expect(Array.isArray(await service.findAll())).toBe(true);
    });
  });

  describe('Create', () => {
    it('should create a new store', async () => {
      const storeDto = new CreateStoreDto();
      storeDto.name = 'Store test';
      storeDto.storeAdress = 'storeaddr';

      let user: User;
      user = {
        id: generateId(),
        email: 'hello@test.io',
        password: 'password',
        role: 'USER',
      };

      let expectedStore: Partial<Store>;
      expectedStore = {
        name: storeDto.name,
        storeAdress: storeDto.storeAdress,
        ownerId: user.id,
      };

      //@ts-ignore
      prismaMock.store.create.mockResolvedValue(expectedStore);

      await expect(
        prisma.store.create({
          data: {
            name: storeDto.name,
            storeAdress: storeDto.storeAdress,
            ownerId: user.id,
            key: GenerateToken(),
          },
        }),
      ).resolves.toEqual(expectedStore);
    });
  });

  describe('Find One', () => {
    it('should find one store', async () => {
      let expectedStore = defaultStore;

      //@ts-ignore
      prismaMock.store.findUnique.mockResolvedValue(expectedStore);

      await expect(
        prisma.store.findUnique({
          where: {
            id: expectedStore.id,
          },
        }),
      ).resolves.toEqual(expectedStore);
    });
  });

  describe('Update', () => {
    it('should update one store', async () => {
      const storeDto = new UpdateStoreDto();
      storeDto.name = 'Store update';
      storeDto.storeAdress = '/store-v2';

      let expectedStore = defaultStore;
      expectedStore.name = storeDto.name;
      expectedStore.storeAdress = storeDto.storeAdress;

      //@ts-ignore
      prismaMock.store.update.mockResolvedValue(expectedStore);

      await expect(
        prisma.store.update({
          where: { id: expectedStore.id },
          data: storeDto,
        }),
      ).resolves.toEqual(expectedStore);
    });
  });
});
