import { forwardRef, Module } from '@nestjs/common';
import { CartModule } from 'src/cart/cart.module';

import { StoreController } from './store.controller';
import { StoreService } from './store.service';

@Module({
  controllers: [StoreController],
  providers: [StoreService],
  exports: [StoreService],
  imports: [forwardRef(() => CartModule)],
})
export class StoreModule {}
