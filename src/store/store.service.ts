import { Injectable } from '@nestjs/common';
import { Store, User } from '@prisma/client';
import { Pagination } from 'src/types/pagination';

import GenerateToken from '../../lib/Token/helper';
import PrismaService from '../prisma/prisma.service';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';
import { StoreNotFoundException } from './exception/store-not-found.exception';

@Injectable()
export class StoreService {
  constructor(private readonly prisma: PrismaService) {}

  public async create(
    createStoreDto: CreateStoreDto,
    owner: User,
  ): Promise<Store> {
    return await this.prisma.store.create({
      data: {
        name: createStoreDto.name,
        storeAdress: createStoreDto.storeAddress,
        ownerId: owner.id,
        key: GenerateToken(),
      },
    });
  }

  public async findAll(pagination?: Pagination) {
    return this.prisma.store.findMany({
      skip: pagination?.skip,
      take: pagination?.take,
    });
  }

  public async findOne(id: string) {
    return await this.validateStore(id);
  }

  public async update(id: string, updateStoreDto: UpdateStoreDto) {
    return await this.prisma.store.update({
      where: { id },
      data: updateStoreDto,
    });
  }

  public async remove(id: string) {
    return await this.prisma.store.delete({ where: { id } });
  }

  public async validateStore(id: string) {
    const store = await this.prisma.store.findUnique({
      where: {
        id,
      },
    });

    if (!store) {
      throw new StoreNotFoundException();
    }

    return store;
  }
}
