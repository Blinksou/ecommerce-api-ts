import { HttpException, HttpStatus } from '@nestjs/common';

export class StoreNotFoundException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Store not Found', code ?? HttpStatus.NOT_FOUND);
  }
}
