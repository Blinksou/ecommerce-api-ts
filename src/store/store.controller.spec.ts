import { Test, TestingModule } from '@nestjs/testing';
import { StoreController } from './store.controller';
import { StoreService } from './store.service';

describe('StoreController', () => {
  let storeController: StoreController;
  let storeService: StoreService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StoreController],
      providers: [StoreService],
    }).compile();

    storeService = module.get<StoreService>(StoreService);
    storeController = module.get<StoreController>(StoreController);
  });

  // describe('findAll', () => {
  //   it('should return an array of store', async () => {
  //     const result = ['test'];
  //     jest.spyOn(storeService, 'findAll').mockImplementation(() => result);

  //     expect(await storeController.findAll()).toBe(result);
  //   });
  // });
});
