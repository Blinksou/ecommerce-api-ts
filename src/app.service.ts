import { Injectable } from '@nestjs/common';

@Injectable()
export default class AppService {
  static getPing(): string {
    return 'Pong !';
  }
}
