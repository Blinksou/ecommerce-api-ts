import { Injectable } from '@nestjs/common';
import { Prisma, User } from '@prisma/client';

import PrismaService from '../prisma/prisma.service';

@Injectable()
export default class UserService {
  constructor(private prisma: PrismaService) {}

  public async findOne(
    userWhereUniqueInput: Prisma.UserWhereUniqueInput,
  ): Promise<User | null> {
    return await this.prisma.user.findUnique({
      where: userWhereUniqueInput,
    });
  }

  public async create(data: Prisma.UserCreateInput): Promise<User> {
    return await this.prisma.user.create({
      data,
    });
  }
}
