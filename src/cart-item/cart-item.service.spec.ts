import { Test, TestingModule } from '@nestjs/testing';
import { CartItemService } from './cart-item.service';
import GenerateToken, { generateId } from '../../lib/Token/helper';
import { prismaMock } from '../../prisma/singleton';
import prisma from '../../prisma/client';
import { Cart, CartItem } from '@prisma/client';
import { AddProductToCartDto } from './dto/add-product-to-cart.dto';
import { UpdateCartItemDto } from './dto/update-cart-item.dto';
import PrismaModule from 'src/prisma/prisma.module';

describe('CartItemService', () => {
  let service: CartItemService;
  let defaultCartItem: CartItem;
  defaultCartItem = {
    id: generateId(),
    cartId: generateId(),
    productId: generateId(),
    quantity: 1,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [CartItemService],
    }).compile();

    service = module.get<CartItemService>(CartItemService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of stores', async () => {
      expect(Array.isArray(await service.findAll())).toBe(true);
    });
  });

  describe('Create', () => {
    it('should create a new cart item', async () => {
      const cartItemDto = new AddProductToCartDto();
      cartItemDto.productId = generateId();
      cartItemDto.quantity = 1;

      let cart: Cart;
      cart = {
        id: generateId(),
        userId: generateId(),
        storeId: generateId(),
      };

      let expectedCartItem: Partial<CartItem>;
      expectedCartItem = {
        productId: cartItemDto.productId,
        quantity: cartItemDto.quantity,
        cartId: cart.id,
      };

      //@ts-ignore
      prismaMock.cartItem.create.mockResolvedValue(expectedCartItem);

      await expect(
        prisma.cartItem.create({
          data: {
            productId: cartItemDto.productId,
            quantity: cartItemDto.quantity,
            cartId: cart.id,
          },
        }),
      ).resolves.toEqual(expectedCartItem);
    });
  });

  describe('Find One', () => {
    it('should find one cart item', async () => {
      let expectedCartItem = defaultCartItem;

      //@ts-ignore
      prismaMock.cartItem.findUnique.mockResolvedValue(expectedCartItem);

      await expect(
        prisma.cartItem.findUnique({
          where: {
            id: expectedCartItem.id,
          },
        }),
      ).resolves.toEqual(expectedCartItem);
    });
  });

  describe('Update', () => {
    it('should update one store', async () => {
      const cartItemDto = new UpdateCartItemDto();
      cartItemDto.quantity = 15;

      let expectedStore = defaultCartItem;
      expectedStore.quantity = cartItemDto.quantity;

      //@ts-ignore
      prismaMock.store.update.mockResolvedValue(expectedStore);

      await expect(
        prisma.cartItem.update({
          where: { id: expectedStore.id },
          data: cartItemDto,
        }),
      ).resolves.toEqual(expectedStore);
    });
  });
});
