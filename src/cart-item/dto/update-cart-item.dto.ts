import { PartialType } from '@nestjs/swagger';

import { AddProductToCartDto } from './add-product-to-cart.dto';

export class UpdateCartItemDto extends PartialType(AddProductToCartDto) {}
