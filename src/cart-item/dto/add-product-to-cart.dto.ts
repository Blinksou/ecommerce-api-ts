import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class AddProductToCartDto {
  // @ApiProperty()
  // @IsNotEmpty()
  // cartId!: string;

  @ApiProperty()
  @IsNotEmpty()
  productId!: string;

  @ApiProperty()
  /* @IsNumber() */
  /*@IsPositive() */
  quantity!: number;
}
