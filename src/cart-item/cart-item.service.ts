import { Injectable } from '@nestjs/common';
import PrismaService from 'src/prisma/prisma.service';
import { Pagination } from 'src/types/pagination';

import { AddProductToCartDto } from './dto/add-product-to-cart.dto';
import { UpdateCartItemDto } from './dto/update-cart-item.dto';
import { CartItemNotFoundException } from './exception/cart-item-not-found.exception';

@Injectable()
export class CartItemService {
  constructor(private readonly prisma: PrismaService) {}

  public async create(
    cartId: string,
    addProductToCartDto: AddProductToCartDto,
  ) {
    return await this.prisma.cartItem.create({
      data: {
        productId: addProductToCartDto.productId,
        quantity: parseInt(addProductToCartDto.quantity.toString()),
        cartId: cartId,
      },
    });
  }

  public async findAll(pagination?: Pagination) {
    return this.prisma.cartItem.findMany({
      skip: pagination?.skip,
      take: pagination?.take,
    });
  }

  public async findOne(id: string) {
    return await this.validateCartItem(id);
  }

  public async update(id: string, updateCartItemDto: UpdateCartItemDto) {
    return await this.prisma.cartItem.update({
      where: { id },
      data: {
        productId: updateCartItemDto.productId,
        quantity: parseInt(updateCartItemDto.quantity.toString()),
      },
    });
  }

  public async remove(id: string) {
    return await this.prisma.cartItem.delete({ where: { id } });
  }

  public async validateCartItem(id: string) {
    const cartItem = await this.prisma.cartItem.findUnique({
      where: {
        id,
      },
      include: { cart: true },
    });

    if (!cartItem) {
      throw new CartItemNotFoundException();
    }

    return cartItem;
  }
}
