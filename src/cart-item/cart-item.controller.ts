import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserRole } from '@prisma/client';
import { Auth } from 'src/auth/decorator/auth.decorator';
import { PermissionRequiredException } from 'src/auth/exception/permission-required.exception';
import RequestWithUser from 'src/auth/request-with-user.interface';
import { CartService } from 'src/cart/cart.service';
import { GetPagination } from 'src/decorators/get-pagination';
import { ProductNotAvailableException } from 'src/product/exception/product-not-available.exception';
import ProductService from 'src/product/product.service';
import { Pagination } from 'src/types/pagination';

import { CartItemService } from './cart-item.service';
import { AddProductToCartDto } from './dto/add-product-to-cart.dto';
import { UpdateCartItemDto } from './dto/update-cart-item.dto';

@Controller('cart-item')
@ApiTags('Cart Item')
export class CartItemController {
  constructor(
    private readonly cartItemService: CartItemService,
    private readonly cartService: CartService,
    private readonly productService: ProductService,
  ) {}

  @Post(':cartId')
  @ApiBearerAuth()
  public async create(
    @Param('cartId') cartId: string,
    @Body() addProductToCartDto: AddProductToCartDto,
  ) {
    await this.cartService.validateCart(cartId);

    const product = await this.productService.validateProduct(
      addProductToCartDto.productId,
    );

    if (
      product.quantity === 0 ||
      product.quantity < addProductToCartDto.quantity
    ) {
      throw new ProductNotAvailableException(
        `Product not available (quantity: ${product.quantity} / asked: ${addProductToCartDto.quantity})`,
      );
    }

    return await this.cartItemService.create(cartId, addProductToCartDto);
  }

  @Get()
  @ApiBearerAuth()
  @Auth('admin')
  public async findAll(@GetPagination() pagination: Pagination) {
    return await this.cartItemService.findAll(pagination);
  }

  @Get(':id')
  @ApiBearerAuth()
  public async findOne(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    // Get cart item
    const cartItem = await this.cartItemService.validateCartItem(id);

    // check if cart item is to current logged user
    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== cartItem.cart.userId
    ) {
      throw new PermissionRequiredException(
        'Cart Item Not Found',
        HttpStatus.NOT_FOUND,
      );
    }

    return cartItem;
  }

  @Patch(':id')
  @ApiBearerAuth()
  public async update(
    @Param('id') id: string,
    @Body() updateCartItemDto: UpdateCartItemDto,
    @Request() req: RequestWithUser,
  ) {
    // Get cart item
    const cartItem = await this.cartItemService.validateCartItem(id);

    // check if cart item is to current logged user
    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== cartItem.cart.userId
    ) {
      throw new PermissionRequiredException(
        'You are not allowed to update this cart item',
      );
    }

    const product = await this.productService.validateProduct(
      updateCartItemDto.productId,
    );

    if (
      product.quantity === 0 ||
      product.quantity < updateCartItemDto.quantity
    ) {
      throw new ProductNotAvailableException(
        `Product not available (quantity: ${product.quantity} / asked: ${updateCartItemDto.quantity})`,
      );
    }

    // update cart item
    return await this.cartItemService.update(id, updateCartItemDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  public async remove(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    // Get cart item
    const cartItem = await this.cartItemService.validateCartItem(id);

    // check if cart item is to current logged user
    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== cartItem.cart.userId
    ) {
      throw new PermissionRequiredException(
        'You are not allowed to remove this cart item',
      );
    }

    // remove cart item
    return await this.cartItemService.remove(id);
  }
}
