import { HttpException, HttpStatus } from '@nestjs/common';

export class CartItemNotFoundException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Cart Item not Found', code ?? HttpStatus.NOT_FOUND);
  }
}
