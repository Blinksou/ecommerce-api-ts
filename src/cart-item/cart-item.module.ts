import { Module } from '@nestjs/common';
import { CartModule } from 'src/cart/cart.module';
import ProductModule from 'src/product/product.module';

import { CartItemController } from './cart-item.controller';
import { CartItemService } from './cart-item.service';

@Module({
  controllers: [CartItemController],
  providers: [CartItemService],
  imports: [CartModule, ProductModule],
})
export class CartItemModule {}
