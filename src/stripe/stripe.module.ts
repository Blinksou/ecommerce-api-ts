import { DynamicModule, Module, Provider } from '@nestjs/common';
import OrderModule from 'src/order/order.module';
import { Stripe } from 'stripe';

import { STRIPE_CLIENT } from './constants';

@Module({})
export default class StripeModule {
  static forRoot(apiKey: string, config: Stripe.StripeConfig): DynamicModule {
    const stripe = new Stripe(apiKey, config);

    const stripeProvider: Provider = {
      provide: STRIPE_CLIENT,
      useValue: stripe,
    };

    return {
      module: StripeModule,
      providers: [stripeProvider],
      exports: [stripeProvider],
      imports: [OrderModule],
      global: true,
    };
  }
}
