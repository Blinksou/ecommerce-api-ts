import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateProductDto {
  @ApiProperty()
  @IsNotEmpty()
  price!: number;

  @ApiProperty()
  @IsNotEmpty()
  quantity!: number;

  @ApiProperty()
  @IsNotEmpty()
  name!: string;

  @ApiProperty()
  @IsNotEmpty()
  storeKey!: string;

  @ApiProperty()
  published?: boolean;
}
