export interface ProductFilter {
  name?: string;
  priceStart?: number;
  priceEnd?: number;
  quantity: number;
}
