import { Injectable } from '@nestjs/common';
import { CartItem, Prisma, Product } from '@prisma/client';
import { StoreNotFoundException } from 'src/store/exception/store-not-found.exception';
import { Pagination } from 'src/types/pagination';

import PrismaService from '../prisma/prisma.service';
import { CreateProductDto } from './dto/request/create-product.dto';
import { UpdateProductDto } from './dto/request/update-product.dto';
import { ProductNotFoundException } from './exception/product-not-found.exception';
import addProductFilter from './lib/add-product-filter';
import { ProductFilter } from './types/product-filter';

@Injectable()
export default class ProductService {
  constructor(private readonly prisma: PrismaService) {}

  public async create(createProductDto: CreateProductDto) {
    const store = await this.prisma.store.findUnique({
      where: { key: createProductDto.storeKey },
    });

    if (store === null) throw new StoreNotFoundException();

    return this.prisma.product.create({
      data: {
        name: createProductDto.name,
        quantity: parseInt(createProductDto.quantity.toString()),
        price: parseInt(createProductDto.price.toString()),
        //@ts-ignore
        storeId: store.id,
      },
    });
  }

  public async findAll(pagination?: Pagination, productFilter?: ProductFilter) {
    let whereParams = addProductFilter({}, productFilter);

    return this.prisma.product.findMany({
      where: whereParams,
      skip: pagination?.skip,
      take: pagination?.take,
    });
  }

  public async findAllByStore(
    storeId: string,
    pagination: Pagination,
    productFilter: ProductFilter,
  ) {
    const store = await this.prisma.store.findUnique({
      where: { id: storeId },
    });

    if (store === null) throw new StoreNotFoundException();

    let whereParams = addProductFilter(
      {
        storeId: storeId,
        published: true,
      },
      productFilter,
    );

    return this.prisma.product.findMany({
      where: whereParams,
      skip: pagination.skip,
      take: pagination.take,
    });
  }

  public async findOne(id: string) {
    return this.prisma.product.findUnique({ where: { id } });
  }

  public async update(id: string, updateProductDto: UpdateProductDto) {
    if (
      // @ts-ignore
      updateProductDto.published === '1' ||
      // @ts-ignore
      updateProductDto.published === 'true'
    ) {
      updateProductDto.published = true;
    } else {
      updateProductDto.published = false;
    }

    return this.prisma.product.update({
      where: { id },
      data: updateProductDto,
    });
  }

  public async remove(id: string) {
    return this.prisma.product.delete({ where: { id } });
  }

  public async validateProduct(id: string) {
    const product = await this.prisma.product.findUnique({
      where: {
        id,
      },
      include: {
        store: true,
      },
    });

    if (!product) {
      throw new ProductNotFoundException();
    }

    return product;
  }
  public async getSuggestedProductsForAnUserOrder(
    order?: Prisma.OrderGetPayload<{
      include: {
        cart: true;
        store: true;
      };
    }>,
    storeId?: string, // should be used only if no order is provided
  ) {
    let lowestPrice = 0;
    let highestPrice = null;

    if (order) {
      const lastCart = await this.prisma.cart.findUnique({
        where: { id: order.cart.id },
        include: {
          items: {
            include: { product: true },
          },
        },
      });

      if (!lastCart) {
        let products = await this.prisma.product.findMany({
          where: {
            quantity: {
              gt: 0,
            },
            storeId: order.store.id,
          },
        });

        products.forEach((product: Product) => {
          if (product.price < lowestPrice) lowestPrice = product.price;
          if (product.price > highestPrice) highestPrice = product.price;
        });
      } else {
        const cartItems = lastCart.items;
        cartItems.forEach((cartItem) => {
          if (cartItem.product.price < lowestPrice)
            lowestPrice = cartItem.product.price;
          if (highestPrice === null || cartItem.product.price > highestPrice)
            highestPrice = cartItem.product.price;
        });
      }
    }

    return await this.prisma.product.findMany({
      where: {
        quantity: {
          gt: 0,
        },
        storeId: storeId ?? order.store.id,
        price: {
          gte: lowestPrice,
          lte: highestPrice ?? 5000,
        },
      },
      take: 10,
    });
  }

  public async updateQuantityAfterOrder(cartItems: Array<CartItem>) {
    for (const cartItem of cartItems) {
      const productToUpdate = await this.prisma.product.findUnique({
        where: {
          id: cartItem.productId,
        },
      });

      if (productToUpdate) {
        const newQuantity = productToUpdate.quantity - cartItem.quantity;
        await this.prisma.product.update({
          where: {
            id: cartItem.productId,
          },
          data: {
            quantity: newQuantity,
          },
        });
      }
    }
  }
}
