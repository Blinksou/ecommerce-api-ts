import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

import { ProductFilter } from '../types/product-filter';

export const GetProductFilter = createParamDecorator(
  (data, ctx: ExecutionContext): ProductFilter => {
    const req: Request = ctx.switchToHttp().getRequest();

    const productFilterParams: ProductFilter = {
      name: null,
      priceStart: 0,
      priceEnd: null,
      quantity: 0,
    };

    productFilterParams.name = req.query.name
      ? req.query.name.toString()
      : null;

    productFilterParams.priceStart = req.query.priceStart
      ? parseInt(req.query.priceStart.toString())
      : 0;

    productFilterParams.priceEnd = req.query.priceEnd
      ? parseInt(req.query.priceEnd.toString())
      : null;

    productFilterParams.quantity = req.query.quantity
      ? parseInt(req.query.quantity.toString())
      : 0;

    return productFilterParams;
  },
);
