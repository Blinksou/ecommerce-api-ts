import { Test, TestingModule } from '@nestjs/testing';

import PrismaModule from '../prisma/prisma.module';
import { CreateProductDto } from './dto/request/create-product.dto';
import { UpdateProductDto } from './dto/request/update-product.dto';
import ProductController from './product.controller';
import ProductService from './product.service';

describe('ProductController', () => {
  let controller: ProductController;
  let productService: ProductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      controllers: [ProductController],
      providers: [ProductService],
    }).compile();

    controller = module.get<ProductController>(ProductController);
    productService = module.get<ProductService>(ProductService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of products', async () => {
      // jest.spyOn(productService, 'findAll').mockImplementation(() => result);
      expect(Array.isArray(await controller.findAll())).toBe(true);
    });
  });

  describe('Create', () => {
    it('should create a new product', async () => {
      const productDto = new CreateProductDto();
      productDto.name = 'ProductTest';
      productDto.price = 10;
      productDto.quantity = 1;
      productDto.storeKey = 's1';

      const result = {
        name: 'ProductTest',
        price: 10,
        quantity: 1,
        storeId: '1',
      };

      expect(await productService.create(productDto)).toEqual(
        expect.objectContaining(result),
      );
    });
  });

  describe('Find One', () => {
    it('should find one product', async () => {
      const result = {
        name: 'ProductTest',
        price: 10,
        quantity: 1,
        storeId: '1',
      };

      expect(await productService.findOne('cky4i4ta30004vs7k6hti2vqx')).toEqual(
        expect.objectContaining(result),
      );
    });
  });

  describe('Update', () => {
    it('should update one product', async () => {
      const productDto = new UpdateProductDto();
      productDto.price = 15;
      productDto.storeId = '1';

      const result = {
        price: 15,
        storeId: '1',
      };

      expect(
        await productService.update('cky4i4ta30004vs7k6hti2vqx', productDto),
      ).toEqual(expect.objectContaining(result));
    });
  });
});
