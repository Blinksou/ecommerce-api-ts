import { Body, Controller, Delete, Get, Param, Patch, Post, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserRole } from '@prisma/client';
import { Auth } from 'src/auth/decorator/auth.decorator';
import { Public } from 'src/auth/decorator/public.decorator';
import { PermissionRequiredException } from 'src/auth/exception/permission-required.exception';
import RequestWithUser from 'src/auth/request-with-user.interface';
import { GetPagination } from 'src/decorators/get-pagination';
import OrderService from 'src/order/order.service';
import { StoreService } from 'src/store/store.service';
import { Pagination } from 'src/types/pagination';

import { GetProductFilter } from './decorator/get-product-filter';
import { CreateProductDto } from './dto/request/create-product.dto';
import { UpdateProductDto } from './dto/request/update-product.dto';
import ProductService from './product.service';
import { ProductFilter } from './types/product-filter';

@Controller('product')
@ApiTags('Product')
export default class ProductController {
  constructor(
    private readonly productService: ProductService,
    private readonly orderService: OrderService,
    private readonly storeService: StoreService,
  ) {}

  @Post('/')
  @ApiBearerAuth()
  public async create(@Body() createProductDto: CreateProductDto) {
    return await this.productService.create(createProductDto);
  }

  @Get('/')
  @ApiBearerAuth()
  @Auth('admin')
  public async findAll(
    @GetPagination() pagination: Pagination,
    @GetProductFilter() productFilter: ProductFilter,
  ) {
    return await this.productService.findAll(pagination, productFilter);
  }

  @Get('/store/:storeId')
  @Public()
  public async findAllByStore(
    @Param('storeId') storeId: string,
    @GetPagination() pagination: Pagination,
    @GetProductFilter() productFilter: ProductFilter,
  ) {
    return await this.productService.findAllByStore(
      storeId,
      pagination,
      productFilter,
    );
  }

  @Get(':id')
  @Public()
  public async findOne(@Param('id') id: string) {
    return await this.productService.findOne(id);
  }

  @Patch(':id')
  @ApiBearerAuth()
  public async update(
    @Param('id') id: string,
    @Body() updateProductDto: UpdateProductDto,
    @Request() req: RequestWithUser,
  ) {
    // get product
    const product = await this.productService.validateProduct(id);

    // check if product is to current logged user
    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== product.store.ownerId
    ) {
      throw new PermissionRequiredException(
        'You are not allowed to update this product',
      );
    }

    return await this.productService.update(id, updateProductDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  public async remove(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    // get product
    const product = await this.productService.validateProduct(id);

    // check if product is to current logged user
    if (
      req.user.role !== UserRole.ADMIN &&
      req.user.id !== product.store.ownerId
    ) {
      throw new PermissionRequiredException(
        'You are not allowed to delete this product',
      );
    }

    return await this.productService.remove(id);
  }

  @Get('store/:storeId/suggestions')
  @ApiBearerAuth()
  public async getSuggestions(
    @Param('storeId') storeId: string,
    @Request() req: RequestWithUser,
  ) {
    await this.storeService.validateStore(storeId);

    const lastOrder = await this.orderService.findLastByUserAndStore(
      req.user.id,
      storeId,
    );

    if (lastOrder) {
      return await this.productService.getSuggestedProductsForAnUserOrder(
        lastOrder,
      );
    }

    return await this.productService.getSuggestedProductsForAnUserOrder(
      null,
      storeId,
    );
  }
}
