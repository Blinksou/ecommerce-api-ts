import { Module } from '@nestjs/common';
import OrderModule from 'src/order/order.module';
import { StoreModule } from 'src/store/store.module';

import ProductController from './product.controller';
import ProductService from './product.service';

@Module({
  controllers: [ProductController],
  providers: [ProductService],
  exports: [ProductService],
  imports: [OrderModule, StoreModule],
})
export default class ProductModule {}
