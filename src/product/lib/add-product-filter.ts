import { ProductFilter } from '../types/product-filter';

export default function addProductFilter(
  whereParams: object,
  productFilter: ProductFilter,
) {
  if (productFilter.name) {
    // @ts-ignore
    whereParams.name = {
      contains: productFilter.name,
    };
  }

  if (productFilter.priceStart) {
    if (productFilter.priceEnd) {
      // @ts-ignore
      whereParams.price = {
        gte: productFilter.priceStart,
        lt: productFilter.priceEnd,
      };
    } else {
      // @ts-ignore
      whereParams.price = {
        gte: productFilter.priceStart,
      };
    }
  }

  if (productFilter.quantity) {
    // @ts-ignore
    whereParams.quantity = {
      gte: productFilter.quantity,
    };
  }

  return whereParams;
}
