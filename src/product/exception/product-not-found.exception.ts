import { HttpException, HttpStatus } from '@nestjs/common';

export class ProductNotFoundException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Product not Found', code ?? HttpStatus.NOT_FOUND);
  }
}
