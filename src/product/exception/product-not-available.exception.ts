import { HttpException, HttpStatus } from '@nestjs/common';

export class ProductNotAvailableException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Product not Available', code ?? HttpStatus.NOT_FOUND);
  }
}
