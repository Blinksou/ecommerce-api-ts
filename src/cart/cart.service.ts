import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import PrismaService from 'src/prisma/prisma.service';
import { Pagination } from 'src/types/pagination';

import { CreateCartDto } from './dto/create-cart.dto';
import { CartAlreadyConfirmedException } from './exception/cart-already-confirmed.exception';
import { CartAlreadyCreatedException } from './exception/cart-already-created.exception';
import { CartNotFoundException } from './exception/cart-not-found.exception';

@Injectable()
export class CartService {
  constructor(private readonly prisma: PrismaService) {}

  public async create(createCartDto: CreateCartDto, owner: User) {
    try {
      const alreadyExistingCart = await this.findCartOfUserByStore(
        createCartDto.storeId,
        owner,
      );

      if (alreadyExistingCart) {
        throw new CartAlreadyCreatedException();
      }
    } catch (e) {
      if (!(e instanceof CartNotFoundException)) {
        throw e;
      }
    }

    return await this.prisma.cart.create({
      data: {
        ...createCartDto,
        userId: owner.id,
      },
    });
  }

  public async confirmCart(user: User, storeId: string) {
    const cart = await this.findCartOfUserByStore(storeId, user);

    if (cart.order) {
      throw new CartAlreadyConfirmedException();
    }

    return await this.prisma.order.create({
      data: {
        cartId: cart.id,
        storeId,
        userId: user.id,
        sessionId: null,
      },
    });
  }

  public async findCartOfUserByStore(storeId: string, user: User) {
    const cart = await this.prisma.cart.findFirst({
      where: { storeId: storeId, userId: user.id, order: null },
      include: { items: true, order: true },
    });

    if (!cart) {
      throw new CartNotFoundException();
    }

    return cart;
  }

  public async findAll(pagination?: Pagination) {
    return this.prisma.cart.findMany({
      skip: pagination?.skip,
      take: pagination?.take,
    });
  }

  public async findOne(id: string) {
    return await this.validateCart(id);
  }

  public async remove(id: string) {
    return await this.prisma.cart.delete({ where: { id } });
  }

  public async validateCart(id: string) {
    const cart = await this.prisma.cart.findUnique({
      where: {
        id,
      },
      include: {
        items: true,
      },
    });

    if (!cart) {
      throw new CartNotFoundException();
    }

    return cart;
  }
}
