import { HttpException, HttpStatus } from '@nestjs/common';

export class CartNotFoundException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Cart not Found', code ?? HttpStatus.NOT_FOUND);
  }
}
