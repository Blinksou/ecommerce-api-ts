import { HttpException, HttpStatus } from '@nestjs/common';

export class CartAlreadyConfirmedException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Cart Already Confirmed', code ?? HttpStatus.NOT_FOUND);
  }
}
