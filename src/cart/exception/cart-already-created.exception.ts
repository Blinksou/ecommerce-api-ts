import { HttpException, HttpStatus } from '@nestjs/common';

export class CartAlreadyCreatedException extends HttpException {
  constructor(msg?: string, code?: HttpStatus) {
    super(msg ?? 'Cart Already Created', code ?? HttpStatus.NOT_FOUND);
  }
}
