import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Request,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserRole } from '@prisma/client';
import { Auth } from 'src/auth/decorator/auth.decorator';
import { PermissionRequiredException } from 'src/auth/exception/permission-required.exception';
import RequestWithUser from 'src/auth/request-with-user.interface';
import { GetPagination } from 'src/decorators/get-pagination';
import { StoreService } from 'src/store/store.service';
import { Pagination } from 'src/types/pagination';

import { CartService } from './cart.service';
import { CreateCartDto } from './dto/create-cart.dto';
import { CartNotFoundException } from './exception/cart-not-found.exception';

@Controller('cart')
@ApiTags('Cart')
export class CartController {
  constructor(
    private readonly cartService: CartService,
    private readonly storeService: StoreService,
  ) {}

  @Post()
  @ApiBearerAuth()
  public async create(
    @Body() createCartDto: CreateCartDto,
    @Request() req: RequestWithUser,
  ) {
    await this.storeService.validateStore(createCartDto.storeId);

    return await this.cartService.create(createCartDto, req.user);
  }

  @Post(':storeId')
  @ApiBearerAuth()
  public async confirmCart(
    @Param('storeId') storeId: string,
    @Request() req: RequestWithUser,
  ) {
    const store = await this.storeService.validateStore(storeId);

    return await this.cartService.confirmCart(req.user, storeId);
  }

  @Get()
  @ApiBearerAuth()
  @Auth('admin')
  public async findAll(@GetPagination() pagination: Pagination) {
    return await this.cartService.findAll(pagination);
  }

  @Get(':id')
  @ApiBearerAuth()
  public async findOne(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    // get cart
    const cart = await this.cartService.validateCart(id);

    // check if cart is to current logged user
    if (req.user.role !== UserRole.ADMIN && req.user.id !== cart.userId) {
      throw new CartNotFoundException();
    }

    return cart;
  }

  @Delete(':id')
  @ApiBearerAuth()
  public async remove(
    @Param('id') id: string,
    @Request() req: RequestWithUser,
  ) {
    // get cart
    const cart = await this.cartService.validateCart(id);

    // check if cart is to current logged user
    if (req.user.role !== UserRole.ADMIN && req.user.id !== cart.userId) {
      throw new PermissionRequiredException(
        'You are not allowed to delete this cart',
      );
    }

    return await this.cartService.remove(id);
  }
}
