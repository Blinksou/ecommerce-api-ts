import { Test, TestingModule } from '@nestjs/testing';
import { Cart, User } from '@prisma/client';
import GenerateToken, { generateId } from '../../lib/Token/helper';
import { prismaMock } from '../../prisma/singleton';
import prisma from '../../prisma/client';
import { CartService } from './cart.service';
import { CreateCartDto } from './dto/create-cart.dto';
import PrismaModule from 'src/prisma/prisma.module';

describe('CartService', () => {
  let service: CartService;
  let defaultStore: Cart;
  defaultStore = {
    id: generateId(),
    userId: generateId(),
    storeId: generateId(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [CartService],
    }).compile();

    service = module.get<CartService>(CartService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of stores', async () => {
      expect(Array.isArray(await service.findAll())).toBe(true);
    });
  });

  describe('Create', () => {
    it('should create a new cart', async () => {
      const cartDto = new CreateCartDto();
      cartDto.storeId = generateId();

      let user: User;
      user = {
        id: generateId(),
        email: 'hello@test.io',
        password: 'password',
        role: 'USER',
      };

      let expectedCart: Partial<Cart>;
      expectedCart = {
        storeId: cartDto.storeId,
        userId: user.id,
      };

      //@ts-ignore
      prismaMock.cart.create.mockResolvedValue(expectedCart);

      await expect(
        prisma.cart.create({
          data: {
            storeId: cartDto.storeId,
            userId: user.id,
          },
        }),
      ).resolves.toEqual(expectedCart);
    });
  });

  describe('Find One', () => {
    it('should find one cart', async () => {
      let expectedCart = defaultStore;

      //@ts-ignore
      prismaMock.cart.findUnique.mockResolvedValue(expectedCart);

      await expect(
        prisma.cart.findUnique({
          where: {
            id: expectedCart.id,
          },
        }),
      ).resolves.toEqual(expectedCart);
    });
  });
});
