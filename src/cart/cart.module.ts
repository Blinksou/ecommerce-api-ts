import { Module } from '@nestjs/common';
import { StoreModule } from 'src/store/store.module';

import { CartController } from './cart.controller';
import { CartService } from './cart.service';

@Module({
  controllers: [CartController],
  providers: [CartService],
  imports: [StoreModule],
  exports: [CartService],
})
export class CartModule {}
