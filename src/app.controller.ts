import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import AppService from './app.service';
import { Public } from './auth/decorator/public.decorator';

@Controller()
@ApiTags('Miscellaneous')
export default class AppController {
  @Get()
  @Public()
  public getHello(): string {
    return '';
  }

  @Get('ping')
  @Public()
  public getPing(): string {
    return AppService.getPing();
  }
}
