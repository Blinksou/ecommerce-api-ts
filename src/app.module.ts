import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';

import AppController from './app.controller';
import AppService from './app.service';
import AuthModule from './auth/auth.module';
import AuthGuard from './auth/guard/auth.guard';
import JwtAuthGuard from './auth/guard/jwt-auth.guard';
import { CartItemModule } from './cart-item/cart-item.module';
import { CartModule } from './cart/cart.module';
import CheckoutModule from './checkout/checkout.module';
import OrderModule from './order/order.module';
import PrismaModule from './prisma/prisma.module';
import ProductModule from './product/product.module';
import { StoreModule } from './store/store.module';
import { MailModule } from './mail/mail.module';
import StripeModule from './stripe/stripe.module';

@Module({
  imports: [
    StripeModule.forRoot(process.env.STRIPE_KEY, { apiVersion: '2020-08-27' }),
    AuthModule,
    StoreModule,
    ProductModule,
    PrismaModule,
    CartModule,
    CartItemModule,
    OrderModule,
    CheckoutModule,
    MailModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export default class AppModule {}
