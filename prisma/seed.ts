import { PrismaClient, UserRole } from '@prisma/client';
import { v4 } from 'uuid';

const prisma = new PrismaClient();

async function main() {
  await prisma.user.upsert({
    where: { email: 'admin@demo.fr' },
    update: {},
    create: {
      email: 'admin@demo.fr',
      password:
        '$argon2i$v=19$m=4096,t=3,p=1$2VizrZEOSieSMajCn3eUmQ$td82UE4mC1G2LjMcL/rDOLCAIr4s3hOYpeguEhHRqd8',
      role: UserRole.ADMIN,
    },
  });

  for (let i = 1; i <= 3; i++) {
    let products = [];

    for (let j = 1; j <= 100; j++) {
      products.push({
        name: `Product ${j} of store ${i}`,
        price: Math.floor(Math.random() * 100),
        quantity: Math.floor(Math.random() * 1000),
        published: true,
      });
    }

    await prisma.user.upsert({
      where: { email: `owner${i}@demo.fr` },
      update: {},
      create: {
        email: `owner${i}@demo.fr`,
        password:
          '$argon2i$v=19$m=4096,t=3,p=1$2VizrZEOSieSMajCn3eUmQ$td82UE4mC1G2LjMcL/rDOLCAIr4s3hOYpeguEhHRqd8',
        role: UserRole.USER,
        stores: {
          create: {
            name: `Store ${i}`,
            storeAdress: `Address ${i}`,
            key: v4(),
            products: {
              create: products,
            },
          },
        },
      },
    });
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
